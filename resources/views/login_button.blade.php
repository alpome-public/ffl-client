<style>

.ffl-button-wrapper {
    /* padding: 20px; */
    text-align: center;
}

.ffl-button {
    background-color: #1572e8;

    border: 0;
    padding: 0;
    display: inline-block;
    box-sizing: border-box;
    overflow: hidden;
    border-radius: 3px;
    background-clip: padding-box;
    position: relative;
    width: 50px;
    height: 50px;
    margin: 0 8px 8px;
}

.ffl-button-icon {
    width: 50px;
    height: 50px;
    top: 0;
    background: rgba(0,0,0,.3);
    position: absolute;
    background-repeat: no-repeat;
    background-size: 50%;
    background-position: 50%;
    background-color: #06418e;
    background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAABU1BMVEUAAAD////////////7+/v+/v7////////////////////////////////////////////////+/v7////////////////+/v78/Pz////////////////////////////////////////////////////////09PT////////////////////////////////+/v7////////////////////////////////////////////////////////////////////////////7+/v////////////////////////////////7+/v////////////////////////9/f3////////////////////////n5+fw8PDw8PD+/v49PT3////y8vLy8vLn5+fy8vJOTk5FRUVERETn5+fn5+fn5+fn5+fn5+fn5+e4uLhSUlL///+0Hs6VAAAAcHRSTlMA+yn5BQKX1f0R0ezg6Yx49B0XGvDcvwsI5D/FoGtDD82nUPZeLSMKqWQzfVq6m00GA9jCt7OvhnJnY1cTkYDu4qKDVCd2SjwgyqyUUzo3H4kxtY5IK2DHL6Rv459lIRoV5dvVxDkXCrR9yL+Zijopk2D0BQAAC2lJREFUeNrs2utbEkEUBvAXAYUFBEHBe6gESRQZmeaN8hqZmpppdrP7vc7//6lvzdbs7JwdN1qe+n2O5+HIvHPO2Q3//XsihcI1dLrnz56+efXy5as3T591oWMVFm9M0U+hxnQcnai8MkC/Geq9iA4THZ/IkIPw7OQgOkd2rEhKPek+dIbqdopc9Y8OI/AKi1dipDcS8OCLgEs6KPgi4LIOCr5LwDsp+C4B76DguwW8g4KvCXh4tlSt1m6GAx58EXDNV+yaSwY4+LqAjyzYDs2FUl4T/D2w+R1wjzHeuZoIXPBFwD1drLl5K1DBFwH3fOIjm4EJPjvgMl7wDwyD73/AFYIUfG7AZUEKPjvgsiAF/xwBD1DwzxHwIAX/HAEPUvDPEfDsfn06vfrgwerGwubJ0t8MvnnAIydjd6wm2SSKo9NHUePg/5WAzwwf9jjWH7Lmdo2D3/aA5+ZdI3U8tmQc/HYGfK83RRpDK1nj4Psd8C04i680iSG1UWlD8CvmHXxxgJiszT/d8aMLPaYdPPuYPOi+YNrxk/OMsBzlycVtt7tjfIA8sarGHb+4A421pnEHPw2RR5mScccP34KraW7AZYdk4LJu1Df8aM14RI/cISPL5qP+KZT2p0xH9JkJMrRtPOrHxqEwaDEDLhslY73Go35Sldi08Yg+Ryqxxura8PDiXIuU5nmjPv9cniV4AZc9IpVLP0fE4RvKWuumO/5UGU42eAHnR4v6a7AZUzW5RNl0x+91zKtFdiH2Dl6wyFlzC7+oqwpuRAx3/NsVyE5iZHO8C65D9pFZIIU0uE7yZLcD2RjZNArg2ompZilI7pGzqS5wXfslax81N2imDK6ZY1VAcpAcqWJyALZcgoQPkOVJGAVbiRTuwMEVUngItmUSXkNmkVACV2WAFBahPr+yVhRckyQ8kS+kaA/n78NvIaEuONgilU1wbdkLqfhUSMQihf4sHKyHSCH/Rwqpg2mcVBJLcFAOk0JsH0zD7oXAMilkglTCd+Fg33igF8Y1hRRJuA+esyYpjcPBIiklK+B5aC/kGiQtEmrgqZHaKhxcJU3lDHUSXkcgaZCwAJ7HpDZwDZJ4itQOwbNmLyTq3qvGwFJJkotpSC6Ti2IULCVNQ5z1PsTtx8hFKietev3kIrwHllskfNJcQKtgmSaJ2+R5ZpGrOlg2SPisyeEyWLbJXT4Hm/Wi9tkQy6pmaDyUBj6tPGkkaz/PfWQ6RRoT3v98TyFbIWEWHJHrpNU6PYlHIvFq2iKtY+9X5RfITklogGOpSRyp69dTxJF8D44DEr5qbrWeKBjKYfJV8x04jjUb4iYJQwUw7JO/Mi88H+hnkO1Ka6rOt3YXIk8HDzWrQuwIDG//SiF7U5qnKNl+EsaDW0iVhNg6ZINJEmrBLaRu/0QWDopeh63vMfJV/zuvo9ZQQXc/d4PhXYZ8NcTqIw9IsOCkm4QbYHh/m3xled6ur8BJ2nNHbJGvDjz3w6vavTWxZLwgmusFQ+W2dl7eIiF2kbcZ+KoEhudh+yf0/2TS9KmWudiJ16daNKz/0dKm46+55KDX2zdchqMRz6tVnnw0AY5lxmx7h4QW2h6SEjiuMDaxNAmJOBi6QuSbTA4Mg0nGsZkkmyo4Rsg3N8GxHmIE+Sjm+Ye+Rb6ZBEed85ELKc9PhOIJ8snAIDhWSIh1cQ5KAyy95JN5sMyyFvJuz2lHX4Z8kYrDc9YbvEegO239STYAz9fkNlR2yeYULNmELwkpGLwMf8SL7iXwjJEP1sDTzewQDelFjd7MiB89hCdaJCF1AUq9/EleODp33hN9ENiPgvLMt1p0C0B7uuJ9SBjfb477NPcSuEZ93Az5EalDbaZH+l9dHJVzxeRgBhLG15sS51H3+ngXXLnrZKwlMuupixSj7Bfnl8G2niRDPTnAKIzb/FceDfB1GVZi9YHvJv+GiFokZPrAV7bIwEgWfPGEhz1sWVo+uc5ukGcTBXiwSTYjXhaXH+2dW3PSUBCAF0O403KRIFigFSm1pQoU7Q1bqNRSrJVqW62jzuj4A/b/PznjE8dNzW5OIumM3ztMvuScPbubnGQCEsLS/DFGJqEg+B5JOjzJOIg4K6GAVBUI7BQey+BAj9SSAvJD5BMBGWUkRRU7mR2ClNenyGUNZHyQNcFGBkkwRYR3YsijByLaBRTdUjNPSbCWkQ1xQ69wZEmbYBu8y6/ftEtoxKxNcOZTjDz06o9IoQ0CiiVBiUEXd3zin0iJOwFpG7T+TtoDwy1/RORX+4U6sjg0Zo8kdOWxCC0omP0zMrKEcWvqh4g8th+TmMVgB2cYFP0SWQE+4Shpukh7Fbjvk0i9AXyqpFkh7hSj5Y+IIVprJyTPYnEm3UYgFzFegYBvdVfbAaA4wBmeSkWkHsKdp7F1YDMlu740RPQ9aiWSo7k6mA2hiMhD3sq8BgE9fgSWiyTJPJfE3rT7zAZ39UVIguH6YBZAQltpHXbb+iLuw4epNGSNEYjYwVlaGiLaJlVyK0VCP6nUQB0NEV0TCxndE+6e6FciEbaJ/IJ8NEHIal25JGENET0Ti3FGBXdvWroi1MQUb9ZDjJITKqyvsPvAaxHcEIcsuhjKM06seCIiLrzPyAMrctZjpFWgLyJsbLSjjF/I6n088FzkSPo0QqEJrrgIKUnwO69Fjll7RRgbVoV7o4dei6wLn9iJtsEl9wyloDnxVuS5CQ5cGYw1RN5vxdOOlyLRnKyYwIcmuCZ/nxmC5SLRLPuBDXmW5bTdOZnni+h71FLIvqEp/bMVDRHiIUtb6yPQooUKz7gi+h7n6v9sgx7mlrok1Xgi+h7tZZwlVQNN1tXjinggspxj3dxQ2AdtXqLCpb5IIs84fwa5capLfKAOrri2CHbH4MADdWBlRuAB+6iwJBeRm0zFLxKTZ8HYkorITaoxdVK1wRPGh+TddxIRuUlfXb1C5+ARu+TddxIRucmEWbfI6aHCtlyE0s3zmoO4zK1v5YMLH8lFKO9NsOVEjbzGBXjIYyTTRFsE7Yd+LsWuI/WLRTxt+lXqdnqosGmCHH4gwaFfzYcFVEhnwWOqIbJG6YrsAWUPxem2vMZSOfPjEY6ygQpT8B6zhwqZc+9bpqM0Klhh0IMVTgZjLZEIEPpRVCjlQRNWgMdE3FYk5tbjgYUKRhl8ooIqVhMoY8Olh7mGKhXwjQiqTDoON5P5HvTP34B/tC10Lk523XlMUWUzDD6S6zKGyI0bjwNUifbBVxqHzm/uDK9JPegqlR6Bz1wachO5R+YEfOeaHJidiczjCFViZ6CP/EUPSx2BSYSzcXkXvIcRX3DSZJrYB4eI7O3rPi4naPVtTJgezQkjvfcJc0iahyMbE5ZHbkuyUVSO9HMjpRMbE4ZHo4skCv5LwmQ81B/bmDh6PEtqeuibkGuC2yY1cfDYiemOK306Q3qbNkdN/uZRW6InA/yGEzWxUKYmt3t8WkaNuOvreoLGMTFZuu2F10/ukw2WFZgTi0h4PybzeS2RuKmSDbF0ZNb3YW5c02ow3aKDEAjPCvSHVZgjl0kkrGTBgX4ECdErmCuNKBLSSs7H+9bgZg7mTK6HFOscbuXqBVIibZg7nQ9ICUWyYEt/O4MEowKB4DqDlORGHAjNzyn7/eABYT1h/x3J+B8a9t++7GUhMNSW0FZlcWYKxyu2GrEDE4LE3iHakX7ZgN+sHqXQjsIlBIx7FtoS6j15e96aZNCWm7lHXUpnsY5C0o8hkFxYKGIyhoBifk4im0ELAszqCvKIRQI4OxReLyODrTIEnnZlgA4U9oK1dtxG/yiJCjR5uStkFw7xFu4vBCgjYTCepm2vxsI3uGvkFwtkbmzk4S5SbG0aM0WHdV2DO8vq1y8/f3z//uPnl68juOuEi8UAFLL/+df8AoAyxlvIoH4fAAAAAElFTkSuQmCC');
}

.ffl-button-text {
    box-sizing: border-box;
    width: 100%;
    padding: 5px 15px 5px 68px;
    line-height: 40px;
    text-align: left;
    text-transform: uppercase;
    font-size: 14px;
    letter-spacing: .7px;
    color: #fff;
    transition: background .3s;
    -webkit-transition: background .3s;
    display: none;
}

@media (min-width: 200px) {
    /* .ffl-button-wrapper {
        padding: 0 32px 25px;
    } */

    .ffl-button {
        width: 100%;
        /* height: auto; */
        margin: 0 0 8px;
        display: block;
    }

    .ffl-button-text{
        display: block;
    }
}
</style>

<div class="ffl-button-wrapper">
    <a href="{{$url}}">
        <button class="ffl-button" type="button">
            <div class="ffl-button-icon"></div>
            <div class="ffl-button-text">
                <span>Login with Faelno</span>
            </div>
        </button>
    </a>
</div>