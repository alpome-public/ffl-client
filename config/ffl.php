<?php

return [
    // Secret Key of this service..
    'secret_key' => env('FFL_SECRET_KEY'),

    // route to redirect the user to after he returns from FFL
    'redirect_home' => env('FFL_RETURN_ROUTE'),

    // get service Id from .env
    'app_id' => env('FFL_APP_ID'),

    // get Server URL
    'server_url' => env('FFL_SERVER_URL'),

    // get Register URL
    'register_url' => env('FFL_REGISTER_URL'),
    
    // Authentication
    'ffl_api_token' => env('FFL_API_TOKEN'),
];
