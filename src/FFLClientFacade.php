<?php

namespace Alpome\FFLClient;

use Illuminate\Support\Facades\Facade;

class FFLClientFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'FFLClient';
    }
}