<?php

namespace Alpome\FFLClient\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Alpome\FFLClient\FFLClient;
use App\User;
use GuzzleHttp\Client;

class FFLController extends BaseController
{
    /**
     * Single page application catch-all route.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //On the service

        //validate the signed URL as usual
        $connect = new FFLClient();
        $valid = $connect->isValidSignature();

        if (!$valid) {
            return redirect()->route(config('ffl.redirect_home'));
        }
        echo 'Valid signature' . PHP_EOL;

        //if valid, get the reference token
        $reference_token = request()->get('reference_token');

        //fetch the data from cache
        $data = cache()->get('ffl_login_' . $reference_token);

        //Exceptions for skies-shoppe
        if (request()->gethost() == "shoppe.grabzrooms.com") {
            $user = \Corals\User\Models\User::where('email', $data['email'])->firstOrFail();
        } else {
            $user = \App\User::where('email', $data['email'])->firstOrFail();
        }
        //login the user and redirect to the homepage
        auth()->login($user);

        return $this->authenticated($reference_token);
    }

    /**
     * The user has been authenticated.
     *
     * @param  User $user
     * @return mixed
     */
    protected function authenticated($reference_token)
    {
        return redirect(config('ffl.redirect_home'). '?reference_token='. $reference_token);
    }

    public function callback(Request $request)
    {
        $post_json = $request->input(0);
        
        //On the service side
        $received_data = json_decode($post_json, true);
        
        //extract signature
        $foreign_signature = $received_data['signature'];

        //remove signature from the array because it was not there when we signed it
        unset($received_data['signature']);

        //convert to json string and generate signature
        $json = json_encode($received_data);

        $secret_key = config('ffl.secret_key');

        $local_signature = hash_hmac('sha256', $json, $secret_key);

        //verify signature
        $is_valid_signature = hash_equals($foreign_signature, $local_signature);

        if (! $is_valid_signature) {
            die('Invalid signature');
        }

        //store data into cache if signature is valid
        cache()->put('ffl_login_' . $received_data['reference_token'], $received_data['data'], now()->addMinutes(5));

        $user = User::where('email', $received_data['data']['email'])->first();

        if ($user) {
            return response()->json(200);
        }
        //get register url
        $register_url = config('ffl.register_url');

        //Here we respond with HTTP 200 to FFL
        return response()->json($register_url, 404);

        //FFL confirms that the JSON data was sent to the service and validated successfully
    }
}
