<?php

namespace Alpome\FFLClient;

use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\InteractsWithTime;

class FFLClient
{
    public function isValidSignature()
    {
        $url = request()->url();

        $original = rtrim($url . '?' . Arr::query(
            Arr::except(request()->query(), 'signature')
        ), '?');
        
        $expires = Arr::get(request()->query(), 'expires');

        $signature = hash_hmac('sha256', $original, $this->getSecretKey());
        
        return hash_equals($signature, (string) request()->query('signature', '')) &&
            ! ($expires && now()->getTimestamp() > $expires);
    }

    public function getSecretKey()
    {
        $secret_key = config('ffl.secret_key');

        if (empty($secret_key)) {
            throw new Exception('Service Secret Key not set.');
        }

        return $secret_key;
    }

    public function loginButton()
    {
        $loginFFL = config('ffl.server_url') . '/login/service/';
        $service = config('ffl.app_id');

        $url = $loginFFL . $service;

        return view('FFL::login_button', compact('url'));
    }
}
